# README #

The Android application to talk to the childhealth repository.

NOTE: I (Dan) think this is an old version of the project.  A newer version is at https://bitbucket.org/jcancelaglez/maventyandroid.

Building
--------

- Download Android Studio.  See https://developer.android.com/sdk/index.html.
- Set up the project.  This involves adding source directories and jars to the build, setting a build directory, creating an Android Virtual Device (AVD), and so on.  See also https://developer.android.com/tools/building/building-studio.html#RunningOnEmulatorStudio and http://stackoverflow.com/a/16625227/34935.
- Run the project on the emulator.
